import caesar
from tkinter import *

def getText():
    return TextArea.get("0.0", END).strip()         #strip permet de ne pas tenir compte du dernier charactère '\n' dans l'encryption

def button_enc_callback():
    text = caesar.encrypt(getText())  
    TextArea.delete("0.0", END)
    TextArea.insert(INSERT, text) 

def button_dec_callback():
    text = caesar.decrypt(getText())
    TextArea.delete("0.0", END)
    TextArea.insert(INSERT, text)

frame = Tk()        
frame.title("Hail Caesar")          #titre de la fenêtre
frame.rowconfigure(0, weight=1)
frame.columnconfigure(1, weight=1)

TextArea = Text(frame)
TextArea.grid(row=0, column=0)
TextArea.insert(INSERT, "abcdef")   #texte par défaut

button_enc = Button(frame, text="Encrypt !", command=button_enc_callback)
button_enc.grid(row=1, column=0)

button_dec = Button(frame, text="Decrypt !", command=button_dec_callback)
button_dec.grid(row=2, column=0)



frame.mainloop()

