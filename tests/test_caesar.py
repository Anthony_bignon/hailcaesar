import unittest 
from caesar import encrypt, decrypt

class TestCaesarFunctions(unittest.TestCase):
    def test_encrypt(self):
        self.assertEqual(encrypt('e'), 'm')
        self.assertEqual(encrypt('a'), 'i')
        self.assertEqual(encrypt('abcd'), 'ijkl')
    def test_decrypt(self):
        self.assertEqual(decrypt('m'), 'e')
        self.assertEqual(decrypt('i'), 'a')
        self.assertEqual(decrypt('ijkl'), 'abcd')
