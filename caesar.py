def encrypt(text):
    result = ""
    for t in text:
        result += chr(ord(t) + 8)
    return result

def decrypt(text):
    result= ""
    for t in text:
        result += chr(ord(t) - 8)
    return result
